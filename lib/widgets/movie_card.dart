import 'package:flutter/material.dart';

class MovieCard extends StatelessWidget {
  final String title;
  final String poster;
  final double rating;
  final Function onTap;
  final String subtitle;

  MovieCard(
      this.title,
      this.poster,
      this.rating,
      this.subtitle,
      this.onTap,
      );

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: ()=>{},
      child: Container(
        padding: EdgeInsets.only(
          top: 10.0,
          right: 12.0,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.spaceBetween,

          children: [
            ClipRRect(
              borderRadius: BorderRadius.all(
                Radius.circular(2.0),
              ),
              child: Stack(
                children: [
                  poster == null
                      ? Container(
                    padding: EdgeInsets.all(10.0),
                    height: 180.0,
                    width: 120.0,
                    child: Icon(
                      Icons.person,
                      size: 100,
                      color: Colors.white24,
                    ),
                  )
                      : Image.network(
                    'http://image.tmdb.org/t/p/original/$poster',
                    fit: BoxFit.cover,
                    width: 150.0,
                    height: 180.0,

                  ),
                  Positioned.fill(
                    child: Material(
                      color: Colors.transparent,
                      child: InkWell(
                        splashColor: Color.fromRGBO(
                          0,
                          0,
                          0,
                          0.3,
                        ),
                        highlightColor: Color.fromRGBO(
                          0,
                          0,
                          0,
                          0.1,
                        ),
                        onTap: ()=>{onTap()},
                      ),
                    ),
                  ),
                ],
              ),
            ),

          ],
        ),
      ),
    );
  }
}

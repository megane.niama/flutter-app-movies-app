/*
import 'dart:convert';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_api/cubit/user_authentication_cubit.dart';
import 'package:movie_app_api/models/user_account.dart';

class LoginForm extends StatefulWidget{
  const LoginForm({Key? key}) : super(key: key);

  @override
  LoginFormState createState() {
    return LoginFormState();
  }


}

class LoginFormState  extends State<LoginForm>{
  final _formKey = GlobalKey<FormState>();
  final _emailTextController = TextEditingController();
  final _passwordTextController = TextEditingController();



  @override
  Widget build(BuildContext context) {
    final userAuthCubit = context.watch<UserAuthenticationCubit>();
    return Form(key:_formKey,
        child: ListView(
          shrinkWrap: true,
          padding: EdgeInsets.symmetric(horizontal: 20),
          children: [
            Padding(
              padding: EdgeInsets.all(20),
              child: Hero(
                  tag: 'hero',
                  child: Center(child:Text('Movies App'))
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 10),
              child: TextFormField(
                keyboardType: TextInputType.emailAddress,
                controller: _emailTextController,
                decoration: InputDecoration(
                    hintText: 'Email',
                    contentPadding: EdgeInsets.symmetric(horizontal: 25, vertical: 20),
                    border: OutlineInputBorder(
                        borderRadius: BorderRadius.circular(50.0)
                    )
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Il faut taper un email !';
                  }
                  return null;
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 20),
              child: TextFormField(
                keyboardType: TextInputType.emailAddress,
                obscureText: true,
                controller: _passwordTextController,
                decoration: InputDecoration(
                    hintText: 'Password',
                    contentPadding: EdgeInsets.symmetric(horizontal: 25, vertical: 20),
                    focusColor: Color.fromRGBO(144, 206, 161, 1),
                    border: OutlineInputBorder(
                      borderRadius: BorderRadius.circular(50.0),
                    )
                ),
                validator: (value) {
                  if (value!.isEmpty) {
                    return 'Il faut rentrer un mot de passe !';
                  }
                  return null;
                },
              ),
            ),
            Padding(
              padding: EdgeInsets.only(bottom: 5),
              child: ButtonTheme(
                height: 56,
                child: RaisedButton(
                  child: Text('Connexion', style: TextStyle(color: Colors.white, fontSize: 20)),
                  color: Color.fromRGBO(144, 206, 161, 1),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50)
                  ),
                  onPressed: () => {
                    if (_formKey.currentState!.validate()) {
                       //id = isUserExistInData(_emailTextController.text,_passwordTextController.text),
                      userAuthCubit.signInUser(UserAccount(id: 1,email: _emailTextController.text, password: _passwordTextController.text)),
                      // If the form is valid, display a snackbar. In the real world,
                      // you'd often call a server or save the information in a database.
                      ScaffoldMessenger.of(context).showSnackBar(SnackBar(content: Text('Tentative de connexion...'),backgroundColor: Color.fromRGBO(144, 206, 161,1)))
                    }
                  },
                ),
              ),
            ),
            FlatButton(
                child: Text('Créer compte', style: TextStyle(color: Colors.grey, fontSize: 16),),
                onPressed: null
            )
          ],
        )


    );
  }

  isUserExistInData(String email, String password) async {
    var jsonResponse;
    await DefaultAssetBundle.of(context).loadString("assets/data/user_account.json").then((value) =>  jsonResponse = jsonDecode(value));
    //final jsonResult = jsonDecode(data);

    for(var p in jsonResponse){
      if(p["email"] == email && p["password"]==password && ModalRoute.of(context)?.settings.name == "/" || ModalRoute.of(context)?.settings.name == "login"){
        //UserAccount user = UserAccount(id:p['id'],email:p['email'],password:p['password']);
        print('true');
      }else if(p["email"] == email && p["password"]==password && ModalRoute.of(context)?.settings.name == "register"){
        print(ModalRoute.of(context)!.settings.name == "login");
      }
    }


  }

}*/

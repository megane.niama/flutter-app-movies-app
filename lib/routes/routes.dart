import 'package:flutter/cupertino.dart';
import 'package:movie_app_api/utils/navigation.dart';
import 'package:movie_app_api/screens/details_screen.dart';
import 'package:movie_app_api/screens/home_screen.dart';
import 'package:movie_app_api/screens/finder_screen.dart';

Route generateRoutes(RouteSettings settings) {
  final args = settings.arguments;

  switch (settings.name) {
    case Navigation.MovieHome:
      return buildRoute(settings, HomeScreen());
    case Navigation.SearchPage:
      return buildRoute(settings, FinderScreen());
    case Navigation.MovieDetail:
      return buildRoute(settings, DetailsScreen(args.hashCode));
    default:
      return buildRoute(settings, HomeScreen());
  }
}

CupertinoPageRoute buildRoute(RouteSettings settings, Widget builder) {
  return CupertinoPageRoute(
    settings: settings,
    builder: (BuildContext context) => builder,
  );
}

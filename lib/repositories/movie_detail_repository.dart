import 'package:dio/dio.dart';


import 'package:movie_app_api/models/movie_detail.dart';

class MovieDetailRepository {
  Dio dio = Dio();

  Future<MovieDetail> getMovieDetail(int movieId) async {
    try {
      Response response = await dio.get("https://api.themoviedb.org/3/movie/$movieId?api_key=fa37448018a4d61b0be0b53061505a00");
      print(response.data);
      return MovieDetail.fromJson(response.data);
    } catch (e) {
      throw e;
    }
  }
}

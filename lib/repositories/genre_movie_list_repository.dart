import 'package:dio/dio.dart';

import 'package:movie_app_api/models/movie_list.dart';
import 'package:movie_app_api/utils/urls.dart';


enum MoviePageType {
  popular,
  upcoming,
  top_rated,
}

class GenreMovieListRepository {
  Dio dio = Dio();

  Future<List<MovieList>> getGenreMovieList(String genreId) async {
    try {
      String type = "popular";
      Response response = await dio.get("https://api.themoviedb.org/3/movie/$type?api_key=fa37448018a4d61b0be0b53061505a00");
      return response.data['results']
          .map<MovieList>((json) => MovieList.fromJson(json))
          .toList();
    } catch (e) {
      throw e;
    }
  }
}

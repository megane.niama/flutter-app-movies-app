import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_api/screens/finder_screen.dart';
import 'package:movie_app_api/screens/home_screen.dart';
import 'package:movie_app_api/utils/constants.dart';
import 'cubit/genremovielist/genre_movie_list_cubit.dart';
import 'cubit/moviecast/movie_cast_cubit.dart';
import 'cubit/moviedetail/movie_detail_cubit.dart';
import 'cubit/searchmovie/search_movie_cubit.dart';

void main() {
  runApp(const MyApp());
}

class MyApp extends StatelessWidget {
  const MyApp({Key? key}) : super(key: key);

  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MultiBlocProvider(
        providers: [
          BlocProvider<GenreMovieListCubit>(
            create: (context) => GenreMovieListCubit()..getGenreMovieList('popular'),
          ),

          BlocProvider<SearchMovieCubit>(
            create: (context) => SearchMovieCubit(),
          ),

          BlocProvider<MovieDetailCubit>(
            create: (context) => MovieDetailCubit(),
          ),
          BlocProvider<MovieCastCubit>(
            create: (context) => MovieCastCubit(),
          ),
        ],
        child: MaterialApp(
          title: 'Flutter Demo',

          theme: ThemeData.dark().copyWith(
            primaryColor: kPrimaryColor,
            scaffoldBackgroundColor: kPrimaryColor,
          ),
          routes: {
            'search': (context) => FinderScreen(),
          },
          //navigatorKey: Navigation.navKey,
          home: HomeScreen(),
        ));
  }
}
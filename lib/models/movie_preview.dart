class MoviePreview {
  final String id;
  final String title;
  final String? imageUrl;
  final String year;
  final bool isFavorite;
  final double rating;
  String overview;

  MoviePreview(
      this.id,
      this.title,
      this.imageUrl,
      this.year,
      this.isFavorite,
      this.overview,
      this.rating,
      );
}
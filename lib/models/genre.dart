class Genre {
  final String name;

  Genre(
      this.name,
      );
}

List<Genre> genres = [
  Genre( 'popular'),
  Genre( 'upcoming'),
  Genre( 'top_rated'),
];

class MovieDetail {
  MovieDetail(
      this.adult,
      this.backdropPath,
      this.budget,
      this.homepage,
      this.id,
      this.imdbId,
      this.originalLanguage,
      this.originalTitle,
      this.overview,
      this.popularity,
      this.posterPath,
      this.releaseDate,
      this.revenue,
      this.runtime,
      this.status,
      this.tagline,
      this.title,
      this.video,
      this.voteAverage,
      this.voteCount,
      this.images,
      );

  bool adult;
  String backdropPath;
  int budget;
  String homepage;
  int id;
  String imdbId;
  String originalLanguage;
  String originalTitle;
  String overview;
  double popularity;
  String posterPath;
  DateTime releaseDate;
  int revenue;
  int runtime;
  String status;
  String tagline;
  String title;
  bool video;
  double voteAverage;
  int voteCount;
  Images images;

  factory MovieDetail.fromJson(Map<String, dynamic> json) => MovieDetail(
    json["adult"],
    json["backdrop_path"],
    json["budget"],
    json["homepage"],
    json["id"],
    json["imdb_id"],
    json["original_language"],
    json["original_title"],
    json["overview"],
    json["popularity"].toDouble(),
    json["poster_path"],
    DateTime.parse(json["release_date"]),
    json["revenue"],
    json["runtime"],
    json["status"],
    json["tagline"],
    json["title"],
    json["video"],
    json["vote_average"].toDouble(),
    json["vote_count"],
    Images.fromJson(json["images"]),
  );

  Map<String, dynamic> toJson() => {
    "adult": adult,
    "backdrop_path": backdropPath,
    "budget": budget,
    "homepage": homepage,
    "id": id,
    "imdb_id": imdbId,
    "original_language": originalLanguage,
    "original_title": originalTitle,
    "overview": overview,
    "popularity": popularity,
    "poster_path": posterPath,
    "release_date": "${releaseDate.year.toString().padLeft(4, '0')}-${releaseDate.month.toString().padLeft(2, '0')}-${releaseDate.day.toString().padLeft(2, '0')}",
    "revenue": revenue,
    "runtime": runtime,
    "status": status,
    "tagline": tagline,
    "title": title,
    "video": video,
    "vote_average": voteAverage,
    "vote_count": voteCount,
    "images": images.toJson(),
  };
}


class Images {
  Images({
    required this.backdrops,
    required this.posters,
  });

  List<Backdrop> backdrops;
  List<Backdrop> posters;

  factory Images.fromJson(Map<String, dynamic> json) => Images(
    backdrops: List<Backdrop>.from(json["backdrops"].map((x) => Backdrop.fromJson(x))),
    posters: List<Backdrop>.from(json["posters"].map((x) => Backdrop.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "backdrops": List<dynamic>.from(backdrops.map((x) => x.toJson())),
    "posters": List<dynamic>.from(posters.map((x) => x.toJson())),
  };
}

class Backdrop {
  Backdrop(
      this.aspectRatio,
      this.filePath,
      this.height,
      this.iso6391,
      this.voteAverage,
      this.voteCount,
      this.width,
      );

  double aspectRatio;
  String filePath;
  int height;
  String iso6391;
  double voteAverage;
  int voteCount;
  int width;

  factory Backdrop.fromJson(Map<String, dynamic> json) => Backdrop(
    json["aspect_ratio"].toDouble(),
    json["file_path"],
    json["height"],
    json["iso_639_1"] == null ? null : json["iso_639_1"],
    json["vote_average"].toDouble(),
    json["vote_count"],
    json["width"],
  );

  Map<String, dynamic> toJson() => {
    "aspect_ratio": aspectRatio,
    "file_path": filePath,
    "height": height,
    "iso_639_1": iso6391 == null ? null : iso6391,
    "vote_average": voteAverage,
    "vote_count": voteCount,
    "width": width,
  };
}
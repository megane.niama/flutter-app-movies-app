class MovieCast {
  MovieCast({
    required this.cast,
  });

  List<Cast> cast;

  factory MovieCast.fromJson(Map<String, dynamic> json) => MovieCast(
    cast: List<Cast>.from(json["cast"].map((x) => Cast.fromJson(x))),
  );

  Map<String, dynamic> toJson() => {
    "cast": List<dynamic>.from(cast.map((x) => x.toJson())),
  };
}

class Cast {
  Cast(
      this.castId,
      this.character,
      this.creditId,
      this.gender,
      this.id,
      this.name,
      this.order,
      this.profilePath,
      );

  int castId;
  String character;
  String creditId;
  int gender;
  int id;
  String name;
  int order;
  String profilePath;

  factory Cast.fromJson(Map<String, dynamic> json) => Cast(
    json["cast_id"],
    json["character"],
    json["credit_id"],
    json["gender"],
    json["id"],
    json["name"],
    json["order"],
    json["profile_path"],
  );

  Map<String, dynamic> toJson() => {
    "cast_id": castId,
    "character": character,
    "credit_id": creditId,
    "gender": gender,
    "id": id,
    "name": name,
    "order": order,
    "profile_path": profilePath,
  };
}

import 'package:carousel_slider/carousel_slider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_api/cubit/moviecast/movie_cast_cubit.dart';
import 'package:movie_app_api/cubit/moviedetail/movie_detail_cubit.dart';
import 'package:movie_app_api/models/movie_cast.dart';
import 'package:movie_app_api/models/movie_detail.dart';
import 'package:movie_app_api/utils/navigation.dart';
import 'package:movie_app_api/utils/urls.dart';
import 'package:movie_app_api/widgets/carousel_item.dart';
import 'package:movie_app_api/widgets/custom_appbar.dart';
import 'package:movie_app_api/widgets/movie_card.dart';
import 'package:provider/src/provider.dart';

class DetailsScreen extends StatefulWidget {
  final int movieId;
  DetailsScreen( this.movieId);

  @override
  _DetailsScreenState createState() => _DetailsScreenState();
}

class _DetailsScreenState extends State<DetailsScreen> {
  ScrollController _scrollController = ScrollController();

  @override
  void initState() {
    context.read<MovieDetailCubit>().getMovieDetail(widget.movieId);

    super.initState();
  }



  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(
        showSearchButton: false,
      ),
      body: SingleChildScrollView(
        child: BlocBuilder<MovieDetailCubit, MovieDetailState>(
            builder: (context, state) {
              if (state is MovieDetailLoadInProgress) {
                return Container(
                  height: MediaQuery.of(context).size.height * 0.8,
                  child: Center(
                    child: CircularProgressIndicator(
                      valueColor: AlwaysStoppedAnimation<Color>(
                        Colors.white,
                      ),
                    ),
                  ),
                );
              } else if (state is MovieDetailLoadSuccess) {
                MovieDetail data = state.movieDetail;
                return Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    Text("dddd"),
                    _featuredImages(data.images.backdrops),
                    _movieTitleInfo(data),
                    _movieDescInfo(data),
                    _movieInfoStatus(data),
                  ],
                );
              } else {
                return Text('NONNN');
              }
            }),
      ),
    );
  }

  Widget _featuredImages(List<Backdrop> data) {
    return CarouselSlider(
      items: data.map((item) {
        return CarouselItem(
          avatar: item.filePath,
          title: '', onTap: ()=>{},
        );
      }).toList(),
      options: CarouselOptions(
        autoPlay: true,
        viewportFraction: 1,
        enlargeCenterPage: false,
      ),
    );
  }

  Widget _movieTitleInfo(MovieDetail data) {
    int year = DateTime.parse(data.releaseDate.toString()).year;
    return Padding(
      padding: const EdgeInsets.all(16.0),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Row(
            children: [
              Expanded(
                flex: 8,
                child: SingleChildScrollView(
                  controller: _scrollController,
                  scrollDirection: Axis.horizontal,
                  child: Text(
                    data.title,
                    overflow: TextOverflow.ellipsis,
                    maxLines: 1,
                    style: TextStyle(
                      fontSize: 28.0,
                      color: Colors.white,
                      fontWeight: FontWeight.w300,
                    ),
                  ),
                ),
              ),
            ],
          ),
        ],
      ),
    );
  }

  Widget _movieDescInfo(MovieDetail data) {
    return Padding(
      padding: EdgeInsets.all(16.0),
      child: Row(
        mainAxisSize: MainAxisSize.max,
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          Expanded(
            flex: 1,
            child: Image.network(
              "${Config.baseImageUrl}${data.posterPath}",
              height: 180.0,
              width: 120.0,
              fit: BoxFit.fill,
            ),
          ),
          Expanded(
            flex: 2,
            child: Padding(
              padding: const EdgeInsets.only(left: 8.0),
              child: Column(
                children: [
                  Container(
                    height: 30,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 8.0),
                    child: Text(
                      data.overview,
                      overflow: TextOverflow.ellipsis,
                      maxLines: 5,
                      style: TextStyle(
                        height: 1.4,
                        color: Colors.white70,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _movieInfoStatus(MovieDetail data) {
    return Padding(
      padding: const EdgeInsets.symmetric(vertical: 16.0),
      child: Row(
        children: [
          Flexible(
            flex: 1,
            child: Center(
              child: Column(
                children: [
                  Icon(
                    Icons.star,
                    color: Colors.amber,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: RichText(
                      text: TextSpan(
                        text: '${data.voteAverage}',
                        style: TextStyle(
                          fontSize: 14.0,
                          color: Colors.white,
                        ),
                        children: <TextSpan>[
                          TextSpan(
                            text: '/10',
                            style: TextStyle(
                              fontSize: 12.0,
                              color: Colors.white70,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 4.0,
                    ),
                    child: Text(
                      'Rating',
                      style: TextStyle(
                        color: Colors.white70,
                        fontSize: 13.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
          Flexible(
            flex: 1,
            child: Center(
              child: Column(
                children: [
                  Icon(
                    Icons.attach_money,
                    color: Colors.green,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 4.0,
                    ),
                    child: Text(
                      'Revenue',
                      style: TextStyle(
                        color: Colors.white70,
                        fontSize: 13.0,
                      ),
                    ),
                  )
                ],
              ),
            ),
          ),
          Flexible(
            flex: 1,
            child: Center(
              child: Column(
                children: [
                  Icon(
                    Icons.local_movies_outlined,
                    color: Colors.redAccent,
                  ),
                  Padding(
                    padding: const EdgeInsets.only(top: 4.0),
                    child: Text(
                      data.status,
                      style: TextStyle(
                        fontSize: 14.0,
                        color: Colors.white,
                      ),
                    ),
                  ),
                  Padding(
                    padding: const EdgeInsets.only(
                      top: 4.0,
                    ),
                    child: Text(
                      'Status',
                      style: TextStyle(
                        color: Colors.white70,
                        fontSize: 13.0,
                      ),
                    ),
                  ),
                ],
              ),
            ),
          ),
        ],
      ),
    );
  }


}

// ignore_for_file: unused_import

import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_api/cubit/searchmovie/search_movie_cubit.dart';
import 'package:movie_app_api/utils/constants.dart';
import 'package:movie_app_api/utils/navigation.dart';
import 'package:movie_app_api/widgets/custom_loading_spin_kit_ring.dart';
import 'package:movie_app_api/widgets/list_tile_search.dart';
import 'package:movie_app_api/widgets/movie_card.dart';
import 'package:movie_app_api/widgets/search_form_field.dart';
import 'package:provider/src/provider.dart';
import 'package:sizer/sizer.dart';

class FinderScreen extends StatefulWidget {
  @override
  _FinderScreenState createState() => _FinderScreenState();
}

class _FinderScreenState extends State<FinderScreen> {
  TextEditingController _textFieldController = TextEditingController();

  @override
  void initState() {
    context.read<SearchMovieCubit>().reset();
    super.initState();
  }

  @override
  void dispose() {
    _textFieldController.dispose();
    super.dispose();
  }

  void _getSearchMovies(String query) {
    context.read<SearchMovieCubit>().getSearchMovies(query);
  }

  void _onPressClear() {
    _textFieldController.clear();
    context.read<SearchMovieCubit>().reset();
  }

  void _onPressMovie(int movieId) {
    if (movieId != null) {
      Navigator.pushNamed(context, Navigation.MovieDetail, arguments: movieId);
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        elevation: 1.0,
        title: _buildSearchField(),
        actions: [
          IconButton(
            icon: Icon(Icons.close),
            onPressed: _onPressClear,
          ),
        ],
      ),
      body: BlocBuilder<SearchMovieCubit, SearchMovieState>(
        builder: (context, state) {
          if (state is SearchMovieLoadInProgress) {
            return Center(
              child: Transform.scale(
                scale: 1,
                child: CircularProgressIndicator(
                  valueColor: AlwaysStoppedAnimation<Color>(
                    Colors.white,
                  ),
                ),
              ),
            );
          } else if (state is SearchMovieLoadSuccess) {
            return ListView.builder(
              itemCount: state.searchMovieResult.length,
              itemBuilder: (context, index) {
                var data = state.searchMovieResult[index];
                return ListTileSearch(
                  data.posterPath,
                  data.title,
                  data.releaseDate,
                      () => _onPressMovie(data.id),
                );
              },
            );
          } else {
            return Container();
          }
        },
      ),
    );
  }

  _buildSearchField() {
    return SearchFormField(
      controller: _textFieldController,
      onChanged: (query) => _getSearchMovies(query),
      placeHolder: 'Search',
    );
  }
}
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:movie_app_api/cubit/genremovielist/genre_movie_list_cubit.dart';
import 'package:movie_app_api/models/genre.dart';
import 'package:movie_app_api/models/movie_list.dart';
import 'package:movie_app_api/screens/details_screen.dart';

import 'package:movie_app_api/utils/navigation.dart';
import 'package:movie_app_api/widgets/custom_appbar.dart';
import 'package:movie_app_api/widgets/movie_card.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen>
    with SingleTickerProviderStateMixin {
  late TabController _tabController;

  @override
  void initState() {
    _tabController = TabController(
      length: genres.length,
      vsync: this,
      initialIndex: 0,
    );
    _tabController.addListener(_handleTabIndex);
    super.initState();
  }

  @override
  void dispose() {
    _tabController.removeListener(_handleTabIndex);
    _tabController.dispose();
    super.dispose();
  }

  void _handleTabIndex() {
    setState(() {});
  }

  void _getGenreListByName(String name) {
    context
        .read<GenreMovieListCubit>()
        .getGenreMovieList (name);
  }

  void _onPressMovie(int movieId) {
    if (movieId != null) {
      Navigator.of(context).push(MaterialPageRoute(builder: (context)=>DetailsScreen(movieId)) );
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: CustomAppBar(),
      body: SingleChildScrollView(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [

            _buildTabbar(),
            BlocBuilder<GenreMovieListCubit, GenreMovieListState>(
              builder: (context, state) {
                if (state is GenreMovieListLoadInProgress) {
                  return Container(
                    width: 800,
                    height: 600,
                    child: Center(
                      child: Transform.scale(
                        scale: 0.7,
                        child: CircularProgressIndicator(
                          valueColor: AlwaysStoppedAnimation<Color>(
                            Colors.white,
                          ),
                        ),
                      ),
                    ),
                  );
                } else if (state is GenreMovieListLoadSuccess) {
                  return Container(
                    child: _buildTabBarView(state.genreMovieLists),
                  );
                } else {
                  return Text(
                    'Failed Get Data Tab',
                    style: TextStyle(
                      color: Colors.white,
                    ),
                  );
                }
              },
            ),
          ],
        ),
      ),
    );
  }

  _buildTabbar() {
    return TabBar(
      controller: _tabController,
      onTap: (index) => _getGenreListByName(index.toString()),
      labelColor: Colors.white,
      indicatorWeight: 3,
      indicatorColor: Colors.white,
      isScrollable: true,
      labelStyle: TextStyle(
        fontSize: 12.0,
        fontWeight: FontWeight.w600,
      ),
      tabs: genres.map(
            (item) {
          return Tab(
            text: item.name.toUpperCase(),
          );
        },
      ).toList(),
    );
  }

  _buildTabBarView(List<MovieList> genreListMovies) {
    return Container(
      height: MediaQuery.of(context).size.height,
      width: MediaQuery.of(context).size.width,
      child: TabBarView(
        physics: NeverScrollableScrollPhysics(),
        controller: _tabController,
        children: genres.map((item) {
          return GridView.builder(
            itemCount: genreListMovies.length,
            itemBuilder: (context, index) {
              MovieList data = genreListMovies[index];
              return MovieCard(
                data.title,
                data.posterPath,
                data.voteAverage,'',
                    () => _onPressMovie(data.id),
              );
            },
            padding: EdgeInsets.symmetric(
              horizontal: 12.0,
            ), gridDelegate: new SliverGridDelegateWithFixedCrossAxisCount(
            crossAxisCount: 2,
            childAspectRatio: 1,
          ),
          );
        }).toList(),
      ),
    );
  }

}

import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:movie_app_api/models/movie_list.dart';
import 'package:movie_app_api/repositories/genre_movie_list_repository.dart';

part 'genre_movie_list_state.dart';

class GenreMovieListCubit extends Cubit<GenreMovieListState> {
  GenreMovieListRepository repository = GenreMovieListRepository();

  GenreMovieListCubit() : super(GenreMovieListInitial());

  Future<void> getGenreMovieList(String name) async {
    try {
      emit(GenreMovieListLoadInProgress());
      final genreMovisLists = await repository.getGenreMovieList(name);
      emit(GenreMovieListLoadSuccess(genreMovisLists));
    } catch (e) {
      throw e;
    }
  }
}
